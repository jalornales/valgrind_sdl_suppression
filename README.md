# Making Valgrind suppressor for SDL2
### Customizable just edit main.cc and Makefile to add LIBS

## First: make the valgrind output with SDL2 leaks

valgrind --leak-check=full --show-reachable=yes --show-leak-kinds=all --error-limit=no --gen-suppressions=all --log-file=valgrind_suppress.log ./a.out

## Second: parse the log with awk script from wxwidgets
cat ./valgrind_suppress.log | ./parse_valgrind_suppressions.sh > sdl_vg.supp

## Third: Test
valgrind --suppressions=./sdl_vg.supp --leak-check=full --show-leak-kinds=all ./a.out

# Sources
[ValgrindSuppression](https://github.com/Rickodesea/ValgrindSuppression)
[wxWidgets How to](https://wiki.wxwidgets.org/Valgrind_Suppression_File_Howto)
[wxWidgets valgrind parser](https://wiki.wxwidgets.org/Parse_valgrind_suppressions.sh)
