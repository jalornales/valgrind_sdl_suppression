CC = g++
CFLAGS = -g3 -W
LIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lSDL2_net

build:
	$(CC) $(CFLAGS) $(LIBS) main.cc

clean:
	rm -rf ./a.out
